This script installs element if it is not installed. With this script you can add profile, add profile with proxy, delete profiles and show existing profiles. 

If you have any questions or something does not work contact at @wildredfox:systemli.org or email at: bumajka@riseup.net.

*P.S If you have any ideas and wishes how to improve the script write me please. With the best wishes and good luck in the fight your wildredfox*

HOWTO
--------
1. Download zip from [git repository](https://0xacab.org/youngblackcat/element_desktop_add_profile_and_proxy/-/archive/master/element_desktop_add_profile_and_proxy-master.zip)
2. Unpack archive
3. Open directory with unpacked archive in Terminal
4. Execute in terminal `chmod +x install_element.sh`
5. Execute in terminal  `./install_element.sh`
