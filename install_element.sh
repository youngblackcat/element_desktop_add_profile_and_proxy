#!/usr/bin/env bash
# maintainer: youngblackcat on 0xacab.org
# version: 1.1
# write for all questions to @wildredfox:systemli.org(element) or mail: bumajka@riseup.net
printf "This script installs element if it is not installed. With this script you can add profile,
        \radd profile with proxy, delete profiles and show existing profiles. If you have any questions 
        \ror something does not work contact at @wildredfox:systemli.org or email at: bumajka@riseup.net.
        \rP.S If you have any ideas and wishes how to improve the script write me please.
        \rWith the best wishes and good luck in the fight your wildredfox\n\n"

check_install_element() {
  if [[ -f /usr/bin/element-desktop ]];
  then
    echo -e "\033[0;96mElement is installed; Before next step this script will close element;\033[0m"
    ps aux | egrep  "/opt/Element/element-desktop$|/opt/Element/element-desktop --profile" | egrep -v grep | awk '{print $2}' | xargs -iX kill X
    choose_next_activity_with_profile;
  else
    printf "\033[0;96mElement is not installed.
            \rAfter this message enter your system password
            \rand element will be installed on your computer \n\n\033[0m"
    sleep 5;
    install_element;
  fi
}

install_element() {
  sudo apt install -y wget apt-transport-https;
  sudo wget -O /usr/share/keyrings/riot-im-archive-keyring.gpg https://packages.riot.im/debian/riot-im-archive-keyring.gpg;
  echo "deb [signed-by=/usr/share/keyrings/riot-im-archive-keyring.gpg] https://packages.riot.im/debian/ default main" | \
  sudo tee /etc/apt/sources.list.d/riot-im.list;
  sudo apt update && sudo apt install -y element-desktop;
  echo -e "\033[0:93mElement was installed\033[0m"
  printf "Element have a deafult profile. If you want use it exit from script"
  choose_next_activity_with_profile;
}

add_profile_element_name() {
  read -p 'enter profile name:' name
  account_name=$(echo $name| awk '{print tolower($0)}' | sed -e 's/_/-/g')
  add_profile_name="--profile=$account_name"
  read -r -p "Are you sure? [Y/n/quit]" response
                response=${response,,} # tolower
  if [[ $response =~ ^(yes|y| ) ]] || [[ -z $response ]]; then
    echo "Your profile will have name: $account_name"
    add_proxy_element
  elif [[ $response =~ ^(no|n| ) ]] ; then
    add_profile_element_name
  elif [[ $response =~ ^(quit|q| ) ]];then
    choose_next_activity_with_profile;
  fi
}

add_proxy_element() {
  read -r -p "Do you want to use proxy-tor? [Y/n/quit]" response
                response=${response,,} # tolower
  if [[ $response =~ ^(yes|y| ) ]] || [[ -z $response ]]; then
    add_proxy="--proxy-server=socks5://127.0.0.1:9150"
    create_bin_file
  elif [[ $response =~ ^(no|n| ) ]] ; then
    add_proxy=""
    create_bin_file
  elif [[ $response =~ ^(quit|q| ) ]];then
    choose_next_activity_with_profile;
  fi
}

create_bin_file(){
  if [[ -z $add_proxy ]]; then
    echo "/usr/bin/element-desktop --profile=$account_name" | sudo tee /usr/bin/element-desktop-$account_name > /dev/null && \
    sudo chmod +x /usr/bin/element-desktop-$account_name
  else
    echo "/usr/bin/element-desktop --profile=$account_name $add_proxy" | sudo tee /usr/bin/element-desktop-$account_name > /dev/null && \
    sudo chmod +x /usr/bin/element-desktop-$account_name
  fi
  create_desktop_icon;
}

create_desktop_icon() {
  if [[ -z $add_proxy ]]; then
    printf "[Desktop Entry]
            \rName=Element $account_name
            \rExec=element-desktop --profile=$account_name %%U
            \rTerminal=false
            \rType=Application
            \rIcon=element-desktop
            \rStartupWMClass=element
            \rComment=A feature-rich client for Matrix.org
            \rMimeType=x-scheme-handler/element;
            \rCategories=Network;InstantMessaging;Chat;" \
    | sudo tee /usr/share/applications/element-desktop-$account_name.desktop > /dev/null
    printf "\n\033[0;93mNew matrix's profile and desktop icon were added.
            \rNew profile will be added in the list after running element with new profile\033[0m\n"
  else
    printf "[Desktop Entry]
            \rName=Element $account_name Tor
            \rExec=element-desktop --profile=$account_name $add_proxy %%U
            \rTerminal=false
            \rType=Application
            \rIcon=element-desktop
            \rStartupWMClass=element
            \rComment=A feature-rich client for Matrix.org
            \rMimeType=x-scheme-handler/element;
            \rCategories=Network;InstantMessaging;Chat;" \
    | sudo tee /usr/share/applications/element-desktop-$account_name.desktop > /dev/null
    printf "\n\033[0;93mNew matrix's profile with proxy to tor network and desktop icon were added.
              \rNew profile will be added in the list after running element with new profile\033[0m\n"
  fi
  choose_next_activity_with_profile;
}

delete_profile_element()  {
  show_existing_profile;
  read -p "enter the number of profile you want to delete(for exit enter q): " number_of_deleting_profile
  if [[ $number_of_deleting_profile == "q" ]]; then
    choose_next_activity_with_profile
  fi
  ### ADD if var = q - > exit; and in message add info about this
  echo "You choose to delete next profile: ${allprofile[$(($number_of_deleting_profile - 1 ))]}"
  read -r -p "Are you shure? [Y/n/quit] " response
                response=${response,,} # tolower
  if [[ $response =~ ^(yes|y| ) ]] || [[ -z $response ]]; then
    deleting_profile;
  elif [[ $response =~ ^(no|n| ) ]] ; then
    delete_profile_element
  elif [[ $response =~ ^(quit|q| ) ]];then
    choose_next_activity_with_profile;
  fi
}

deleting_profile() {
  if [[ ${allprofile[$(($number_of_deleting_profile - 1 ))]} == Element ]]; then
    rm -rf ~/.config/${allprofile[$(($number_of_deleting_profile - 1 ))]} ;
    sudo rm /usr/share/applications/element-desktop.desktop;
    choose_next_activity_with_profile
  else
    rm -rf ~/.config/${allprofile[$(($number_of_deleting_profile - 1 ))]};
    short_name_deleting_profile=$(echo ${allprofile[$(($number_of_deleting_profile - 1 ))]} | awk '{print tolower($0)}' | awk 'BEGIN{FS="-"; OFS="-"} {$1="";print $0}')
    sudo rm /usr/share/applications/element-desktop$short_name_deleting_profile.desktop;
    sudo rm /usr/bin/element-desktop$short_name_deleting_profile;
    choose_next_activity_with_profile
  fi
  echo -e "\033[0:93mProfile was deleted\033[0m"
}

show_existing_profile () {
  allprofile=()
  allprofile+=( $(cd ~/.config/ ; ls | grep Element) )
  for i in ${!allprofile[*]};
    do printf "%1d) %s\n" $(( $i + 1 )) ${allprofile[$i]};
  done
  printf "\n"
}

choose_next_activity_with_profile() {
  printf "\nYou are to do next activity with profile:
          \r1) add profile
          \r2) delete profiles
          \r3) show existing profiles
          \r4) exit \n \n"
  read -p "Your chosen activity: " next_step
  case $next_step in
    "add profile" | add | 1)
      echo -e "\n\033[0;92mAfter a few steps you will add a new profile, follow the instructions\033[0m\n"
      add_profile_element_name
      ;;
    "delete profiles" | "delete profile" | "delete" | 2 )
      printf "\n\033[0;91mAfter a few steps you will delete a choosen profile.
      \rReminder: new profile will be added in the list after runing element with new profile\033[0m\n\n"
      delete_profile_element;
      ;;
      "show existing profile" | "show profile" | "show profiles" | show | 3)
        echo -e "\n\033[0;93mIf you add new profile, it will be printed in the list after starting element with new profile\033[0m\n"
        show_existing_profile;
        choose_next_activity_with_profile;
      ;;
    exit | 4)
      echo "exit"
      exit;
    ;;
esac
}

check_install_element
